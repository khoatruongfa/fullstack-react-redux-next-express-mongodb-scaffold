# Fullstack scaffolding project
A base project with utilize configuration for building web application as the role Fullstack developer.

What I have covered:
- Next & Express
- MongoDB
- GrapQL
- Apollo Server & Client

## Installation
```
npm install
```

## How magical is it?
- Development
```
npm run dev
```
- Test
```
npm test
```

## Auth middleware
- `auth.requireAuth` require authenticated user to access this route (API)
- `auth.requireRole([ 'admin', 'user' ])` require autheticated user and also have one of these roles.

## Handle restrict routing in backend
Edit `restrict.route.js` file.

## Troubleshooting
### Can not run `.sh` file on Window platform
```
> . ./development.env.sh && nodemon --inspect -e js,graphql,json .

'.' is not recognized as an internal or external command,
operable program or batch file.
npm ERR! code ELIFECYCLE
```
Find out the shell setting in your npm config
```
npm config ls -l | grep shell
```
Just add or edit npmrc file to `C:\Users\%your_user_name%\AppData\Roaming\npm`. We override the default configs by replace the window shell to git bash.
```
shell = "C:\\Program Files (x86)\\git\\bin\\bash.exe"
```
