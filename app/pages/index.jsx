import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import { ProductList } from '../components/';
import { DefaultLayout } from '../components/';

import * as productActionCreators from '../actions/products.action';

const Home = ({ addProduct }) => {
  const handleOnSubmit = e => {
    e.preventDefault();
    e.stopPropagation();

    const { currentTarget: formElement } = e;
    const { _id, name, price } = formElement.elements;

    addProduct({
      _id: _id.value,
      name: name.value,
      price: price.value
    });

    formElement.reset();
  };

  return (
    <DefaultLayout>
      <div className="Home">
        <h1 className="title"><FormattedMessage id="home.title" /></h1>
        <ProductList></ProductList>

        <div className="product-board">
          <form onSubmit={handleOnSubmit}>
            <label htmlFor="_id">
              ID: <input type="text" name="_id" id="_id" />
            </label>
            <label htmlFor="name">
              NAME: <input type="text" name="name" id="name" />
            </label>
            <label htmlFor="price">
              PRICE: <input type="text" name="price" id="price" />
            </label>
            <button type="submit">Add</button>
          </form>
        </div>
      </div>
    </DefaultLayout>
  );
}

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProp = dispatch => bindActionCreators(productActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProp)(Home);