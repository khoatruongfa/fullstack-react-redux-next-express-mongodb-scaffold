export const ADD_PRODUCT = 'ADD_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

export const addProduct = ({ _id, name, price }) => {
  return {
    type: ADD_PRODUCT,
    _id,
    name,
    price
  }
}

export const deleteProduct = ({ _id }) => {
  return {
    type: DELETE_PRODUCT,
    _id
  }
}

export const updateProduct = ({ _id, name, price }) => {
  return {
    type: UPDATE_PRODUCT,
    _id,
    name,
    price
  }
}