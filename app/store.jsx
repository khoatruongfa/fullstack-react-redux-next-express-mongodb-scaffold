import { createStore, compose } from 'redux';

import rootReducer from './reducers/';

import { products } from './mocks/';

const defaultState = {
  products,
  app: {
    locale: null // retrieve from client (navigator.language) if not set
  }
};

const store = createStore(rootReducer, defaultState);

if(module.hot) {
  module.hot.accept('./reducers/',() => {
    const nextRootReducer = require('./reducers/').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;