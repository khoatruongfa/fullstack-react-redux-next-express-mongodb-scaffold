import React from 'react'
import Link from 'next/link'

const links = [
  { href: '#', label: 'Next' },
  { href: '#', label: 'React' },
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`
  return link
})

const Nav = () => (
  <nav>
    <ul>
      <li>
        <Link href="/">
          <a>Home</a>
        </Link>
      </li>
      {links.map(({ key, href, label }) => (
        <li key={key}>
          <a href={href}>{label}</a>
        </li>
      ))}
    </ul>

    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      nav {
        text-align: center;
      }
      ul {
        display: flex;
        justify-content: space-between;
      }
      nav > ul {
        margin: 0;
        padding: 16px 240px;
      }
      li {
        display: flex;
        padding: 6px 8px;
      }
      a {
        color: #F0EFF4;
        text-decoration: none;
        font-size: 24px;
        font-weight: 700;
      }
    `}</style>
  </nav>
)

export default Nav
