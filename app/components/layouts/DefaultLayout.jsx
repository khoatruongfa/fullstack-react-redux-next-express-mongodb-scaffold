import React from 'react';
import Head from 'next/head';
import Nav from '../Nav';

export default function DefaultLayout(props) {
  return (
    <div>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      {props.children}
    </div>
  );
}