import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ProductItem from './ProductItem';
import * as productActionCreators from '../../actions/products.action';

const ProductList = ({ products, deleteProduct }) => {
  return (
    <div className="ProductList">
      {
        products.map(({ _id, name, price }) =>
          <ProductItem key={_id} _id={_id} name={name} price={price} handleOnDelete={() => deleteProduct({ _id })}></ProductItem>
        )
      }
    </div>
  );
}

const mapStateToProps = state => {
  return {
    products: state.products
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(productActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);