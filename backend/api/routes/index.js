const express = require('express');
const router = express.Router();

const authRoute = require('./auth.route');
const author = require('../../../author.json');

router.use('/author', (req, res) => res.json(author))

router.use('/auth', authRoute);

module.exports = router;