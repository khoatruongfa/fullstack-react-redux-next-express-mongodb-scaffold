const glue = require('schemaglue');

const schemaDirectives = require('./directives/');

const { schema, resolver } = glue('backend/graphql', {
  js: '**/*.js'
});

module.exports = {
  typeDefs: schema,
  resolvers: resolver,
  schemaDirectives
}