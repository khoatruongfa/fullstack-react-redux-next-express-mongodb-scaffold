const mongo = require('./mongo.config');
const serverConfigs = require('./server.config');
const logger = require('./logger.config');
const { to } = require('await-to-js');
module.exports = {
  initialize: async () => {
    const [err, db] = await to(mongo.connect());
    if (!err) {
      logger.info('Connected to MongoDB');
      return { db, logger };
    }
    return { db: {}, logger };
  }
}
